from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from collections import defaultdict
import sys
sys.path.insert(0, 'C:\Users\Nick\Documents\Trading')
sys.path.insert(0, 'C:\Users\Nick\Documents\Visual Studio 2015\Projects\Penchovski\Penchovski')

from utility import combinations,non_dominated_sort
import threading
import numpy as np
import copy
from ViennaInterface import Ractip, Tup2Str

#Some code required for flask
#Instantiate app and db  
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

#Database model definition    

class Individuals(db.Model): 
    idInDb                 = db.Column(db.Integer, primary_key=True)
    id                     = db.Column(db.Integer)   #Foreign key
    trial                  = db.Column(db.Integer)   #Foreign key
    fitnesses              = db.Column(db.PickleType)
    popType                = db.Column(db.Integer)
    curGen                 = db.Column(db.Integer)

    def __init__(self,id,trial,fitnesses,popType,curGen):
        self.id                     = id
        self.trial                  = trial
        self.fitnesses              = fitnesses
        self.popType                = popType
        self.curGen                 = curGen


class Trials(db.Model):
    id                         = db.Column(db.Integer, primary_key=True)
    numInds                    = db.Column(db.Integer)
    numGens                    = db.Column(db.Integer)

    selectionMethod            = db.Column(db.Integer)
    selectionParams            = db.Column(db.PickleType)
    survivorSelectionMethod    = db.Column(db.Integer)
    mutationProb               = db.Column(db.Float)
    mutationRate               = db.Column(db.Float)
    selectedFitnesses          = db.Column(db.PickleType)
    availableFits              = db.Column(db.PickleType)
    fitnessParams              = db.Column(db.PickleType)
    multiprocessing            = db.Column(db.Integer)

    colorStrings               = db.Column(db.PickleType)
    designType                 = db.Column(db.String)

    metaLevel                  = db.Column(db.Integer) #This is required for trials summary so that we can filter out the non highest level trials


    def __init__(self, numInds, numGens, selectionMethod, selectionParams, survivorSelectionMethod,mutationProb, mutationRate, selectedFitnesses, availableFits, fitnessParams, multiprocessing, colorStrings, designType, metaLevel):                   
        self.numInds                    = numInds                     
        self.numGens                    = numGens                
        self.selectionMethod            = selectionMethod             
        self.selectionParams            = selectionParams 
        self.survivorSelectionMethod    = survivorSelectionMethod       
        self.mutationProb               = mutationProb                
        self.mutationRate               = mutationRate                     
        self.selectedFitnesses          = selectedFitnesses           
        self.availableFits              = availableFits               
        self.fitnessParams              = fitnessParams               
        self.multiprocessing            = multiprocessing             
        self.colorStrings               = colorStrings
        self.designType                 = designType
        self.metaLevel                  = metaLevel

###TO DO -> NN trials, amp/conv trials, coupling trials for the specific trial information

class Misc(db.Model):
    id           = db.Column(db.Integer, primary_key=True)
    trialId      = db.Column(db.Integer)
    rzInputPairs = db.Column(db.PickleType)

    def __init__(self, trialId, rzInputPairs):
        self.trialId      = trialId
        self.rzInputPairs = rzInputPairs

class DisplayOptions(db.Model): 
    id                  = db.Column(db.Integer, primary_key=True)
    popTypes            = db.Column(db.PickleType)
    dispFits            = db.Column(db.PickleType)
    dispFitsIdx         = db.Column(db.PickleType)
    metaLevel           = db.Column(db.Integer)

    def __init__(self,popTypes, dispFits,dispFitsIdx, metaLevel):
        self.popTypes    = popTypes
        self.dispFits    = dispFits 
        self.dispFitsIdx = dispFitsIdx
        self.metaLevel   = metaLevel


class MetaResults(db.Model):
    id          = db.Column(db.Integer, primary_key=True)
    indId       = db.Column(db.Integer) #Foreign key IN
    ampTrialId  = db.Column(db.Integer) #Foreign key OUT
    convTrialId = db.Column(db.Integer) #Foreign key OUT
    trial       = db.Column(db.Integer)

    def __init__(self,indId,ampTrialId, convTrialId, trial):
        self.indId       = indId
        self.ampTrialId  = ampTrialId
        self.convTrialId = convTrialId
        self.trial       = trial

class MonomerFoldingResults(db.Model):
    idInDb    = db.Column(db.Integer, primary_key=True)
    id        = db.Column(db.Integer) #This is its ID in the individual. It is not currently being used.
    indId     = db.Column(db.Integer) #Foreign key
    name      = db.Column(db.PickleType)
    seq       = db.Column(db.String)
    phenotype = db.Column(db.String)
    trial     = db.Column(db.Integer) #TO SEE whether I can remove this

    def __init__(self,id,indId,name, seq, phenotype,trial):
        self.id        = id
        self.indId     = indId
        self.name      = name
        self.seq       = seq
        self.phenotype = phenotype
        self.trial     = trial

class DimerFoldingResults(db.Model):
    idInDb      = db.Column(db.Integer, primary_key=True)
    id          = db.Column(db.Integer)
    indId       = db.Column(db.Integer) #Foreign key    
    name1       = db.Column(db.PickleType)
    seq1        = db.Column(db.String)
    name2       = db.Column(db.PickleType)
    seq2        = db.Column(db.String)
    phenotype   = db.Column(db.String)
    k           = db.Column(db.Float)
    trial       = db.Column(db.Integer)

    def __init__(self,id, indId, name1, seq1, name2, seq2, phenotype,k, trial):
        self.id = id 
        self.indId = indId
        self.name1 = name1
        self.seq1 = seq1
        self.name2 = name2
        self.seq2 = seq2
        self.phenotype = phenotype
        self.k = k
        self.trial = trial



#global vars
trialToViewPrev = -1
genToViewPrev = -1
indIdPrev = -1
popTypeToViewPrev = -1
trialInstanceToViewPrev = -1
individualsAll = list()
retrievedMonomers = list()
retrievedDimers = list()
colorString = ''
fronts = dict()
designType = ''
displayOptionsPrev = DisplayOptions([],[],[],0)


def SliderQuery(numValues,sliderValue):
    cutoffs = [i/float(numValues) for i in range(1,numValues+1)]
    for resultToView,cutoff in enumerate(cutoffs):
        if sliderValue < cutoff:
            break
    return resultToView


@app.route('/displayOptions')
def RenderDisplayOptions():

    if trialToViewPrev != -1:           t = Trials.query.filter(Trials.id == trialToViewPrev).all()[0].availableFits
    else:                               t = []

    if trialInstanceToViewPrev != -1:   ti = Trials.query.filter(Trials.id == trialInstanceToViewPrev).all()[0].availableFits
    else:                               ti = []

    return render_template('displayOptions.html', availableFits = t, availableFitsInstance = ti)

@app.route('/processDisplayForm', methods=['POST'])
def processDisplayForm():
    dropDownToPopTypes = {0 : [0], 1 : [1], 2 : [2], 3: [1,2] }

    trial         = Trials.query.filter(Trials.id == trialToViewPrev).all()[0]
    availableFits = trial.availableFits

    dispFits      = list() #The displayed fitnesses list also acts as a front name
    dispFitsIdx   = list()
    for idx,p in enumerate(availableFits):
        if p in request.form.values():
            dispFits.append(request.form[p])
            dispFitsIdx.append(idx)
    popTypes = dropDownToPopTypes[int(request.form['popTypesDropDown'])]
    DisplayOptions.query.filter_by(metaLevel=0).delete()
    displayOption = DisplayOptions(popTypes, dispFits, dispFitsIdx, 0)
    db.session.add(displayOption)

    if trialInstanceToViewPrev != -1:
        availableFitsInstance  = Trials.query.filter(Trials.id == trialInstanceToViewPrev).all()[0].availableFits
        dispFitsInstance       = list() #The displayed fitnesses list also acts as a front name
        dispFitsIdxInstance    = list()
        for idx,p in enumerate(availableFitsInstance):
            if p in request.form.values():
                dispFitsInstance .append(request.form[p])
                dispFitsIdxInstance .append(idx)
        popTypesInstance = dropDownTopPopType[int(request.form['popTypesDropDownInstance'])]
        DisplayOptions.query.filter_by(metaLevel=1).delete()
        displayOptionInstance = DisplayOptions(popTypesInstance, dispFitsInstance, dispFitsIdxInstance, 1)
        db.session.add(displayOptionInstance)

    db.session.commit()

    return RenderTrialsSummary()


#This loads the form to create a trial
@app.route("/createTrial")
def RenderCreateTrial():
    return render_template('createTrial.html', title='Create Trial')

#This is called once we post the form above
#It starts running the EA and then renders the trials summary page
@app.route('/processCreateTrialForm', methods=['POST'])
def ProcessCreateTrialForm():
    #These are fields common to all design types
    numInd                     = int(request.form['numInd'])
    numGen                     = int(request.form['numGen'])
    mutationRate               = int(request.form['mutationRate'])
    selectionMethod            = int(request.form['selectionMethod'])
    selectionParams            = ReadListFromForm(str(request.form['selectionParams']),'float')
    selectedFitnesses          = ReadListFromForm(str(request.form['selectedFitnesses']),'string')
    survivorSelectionMethod    = int(request.form['survivorSelectionMethod'])
    availableFits              = ReadListFromForm(str(request.form['availableFits']),'string')
    fitnessParams              = ReadListFromForm(str(request.form['fitnessParams']),'int')
    
    a                          = float(request.form['a'])
    b                          = float(request.form['b'])
    t                          = float(request.form['t'])
    u                          = float(request.form['u'])
    accNum                     = int(request.form['accNum'])    
    foldingParams              = [a,b,t,u,accNum]

    designType                 = str(request.form['designType'])

    EAParams = (numInd,numGen,selectionMethod, selectionParams,survivorSelectionMethod,1.0,mutationRate,selectedFitnesses,availableFits, fitnessParams, 3)

    if designType == 'Amplifier' or designType == 'Converter' or designType == 'NN' or designType == 'Amplifier-two-stage':        

        #Insert a default value for displayOptions
        db.session.add(DisplayOptions([1], availableFits,range(len(availableFits)),0))
        db.session.commit()

        if designType == 'NN':
            targetWeightMatrixL1 = np.array([[0.5]])
            targetWeightMatrices = [targetWeightMatrixL1]
            biases = np.array([[0]]) #Outer bracket is layer, innner bracket is neuron
            designParams = [[targetWeightMatrices,biases]]
        else:
            designParams = [{}]

        currentTrial = RNAEA.RNAEA(designType,designParams,foldingParams)
        InsertTrial(EAParams+(currentTrial.colorStrings,designType,0))

        t = threading.Thread(target=currentTrial.run, args = EAParams)
        t.start()


    elif designType == 'Coupling':
        numIndMeta                     = int(request.form['numIndMeta'])
        numGenMeta                     = int(request.form['numGenMeta'])
        mutationRateMeta               = int(request.form['mutationRateMeta'])
        selectionMethodMeta            = int(request.form['selectionMethodMeta'])
        selectionParamsMeta            = ReadListFromForm(str(request.form['selectionParamsMeta']),'float')
        selectedFitnessesMeta          = ReadListFromForm(str(request.form['selectedFitnessesMeta']),'string')
        survivorSelectionMethodMeta    = int(request.form['survivorSelectionMethodMeta'])
        availableFitsMeta              = ReadListFromForm(str(request.form['availableFitsMeta']),'string')
        fitnessParamsMeta              = ReadListFromForm(str(request.form['fitnessParamsMeta']),'int')

        metaEAParams = (numIndMeta,numGenMeta,selectionMethodMeta, selectionParamsMeta,survivorSelectionMethodMeta,1,mutationRateMeta,selectedFitnessesMeta,availableFitsMeta, fitnessParamsMeta, 3) 

        #Insert a default value for displayOptions
        db.session.add(DisplayOptions([1], availableFitsMeta,range(len(availableFitsMeta)),0))
        db.session.add(DisplayOptions([1], availableFits,range(len(availableFits)),1))
        db.session.commit()

        currentTrial = CouplingEA.CouplingEA(EAParams,foldingParams) #We pass it the instance EA params
        InsertTrial(metaEAParams+('',designType,0)) #!! The coupling meta trial doesn't have any color strings

        t = threading.Thread(target=currentTrial.run, args = metaEAParams)
        t.start()

    return RenderTrialsSummary()


#This page shows a summary of the trials. It is rendered by the handle_data function above. I never really access it by its app.route
@app.route("/trials")
def RenderTrialsSummary():
    allTrials = Trials.query.filter_by(metaLevel = 0).all()
    links = ["/trialDetails?trial=" + str(trial.id) for trial in allTrials]
    return render_template('trials.html', title='Trials', trials = allTrials, links = links)


#We get to this page by clicking on a trial number in trials summary
@app.route("/trialDetails",  methods=['GET'])
def RenderTrialDetails():
    trialToView = request.args.get('trial',0,type=int)  
    print trialToView
    return render_template('trialDetails.html', title='Trial Details', trialToView = trialToView)

#The page rendered above will continuously call this function to update the contents of the page
@app.route("/trialDetails_JSON",  methods=['GET'])
def ViewTrialIndividuals():
    currentDisplayOptions = DisplayOptions.query.all()[-1]
    dispFits              = currentDisplayOptions.dispFits
    popTypesToView        = currentDisplayOptions.popTypes
    dispFitsIdx           = currentDisplayOptions.dispFitsIdx

    allFitFuncAvgAllGen   = list()
    allFitFuncMaxAllGen   = list()
    trialToView           = request.args.get('trial',0,type=int)  
    trialIndividuals      = Individuals.query.filter(Individuals.trial == trialToView).filter(Individuals.popType.in_(popTypesToView)).all()
    numFitFuncs           = len(dispFits)

    if trialIndividuals != []:
        numGenerations = max([trialIndividual.curGen for trialIndividual in trialIndividuals]) + 1
        for fitFuncId in range(numFitFuncs):
            fitFuncAvgAllGen = list()
            fitFuncMaxAllGen = list()
            for genId in range(numGenerations):
                fitFuncValuesCurGen = [ind.fitnesses[dispFitsIdx[fitFuncId]] for ind in trialIndividuals if ind.curGen == genId]
                fitFuncAvgCurGen = sum(fitFuncValuesCurGen)/float(len(fitFuncValuesCurGen))
                fitFuncAvgAllGen.append(fitFuncAvgCurGen)
                fitFuncMaxCurGen = max(fitFuncValuesCurGen)
                fitFuncMaxAllGen.append(fitFuncMaxCurGen)
            allFitFuncAvgAllGen.append(fitFuncAvgAllGen)
            allFitFuncMaxAllGen.append(fitFuncMaxAllGen)
        json_test = jsonify((allFitFuncAvgAllGen,allFitFuncMaxAllGen,dispFits))
        return json_test
    return render_template('trialDetails.html', title='Trial Details', trialToView = trialToView)






#############################


#We get to this page by clicking on a dot in trial details
@app.route("/viewFittestFront",  methods=['GET'])
def RenderViewFittestFront():
    ###TO DO -> Add meta trial
    trialToView = request.args.get('trial',0,type=int)  
    genToView = request.args.get('gen',0,type=int)   
    return render_template('viewFittestFront.html', title='View Fittest Front', trialToView = trialToView, genToView = genToView)


def RetrieveFittestFront(inds,trialToView, genToView, popTypesToView, dispFits, dispFitsIdx):
    #Only perform a non dominated sort if it hasn't been performed before.
    frontKey = (trialToView, genToView, tuple(popTypesToView), tuple(dispFits))
    if frontKey not in fronts.keys():
        dataset = list()
        for ind in inds:
            dataset.append([ind.fitnesses[i] for i in dispFitsIdx])
        curFronts = non_dominated_sort(dataset)
        fronts[frontKey] = curFronts
    return fronts[frontKey][0]
        

@app.route("/viewFittestFront_JSON",  methods=['GET'])
def ViewFittestFrontJSON():

    global trialToViewPrev
    global indIdPrev
    global trialInstanceToViewPrev
    global designType
    global displayOptionsPrev
    global genToViewPrev

    trialToView  = request.args.get('trial',0,type=int) #This is template generated hardcoded into web page

    ##### Display options
    displayOptions        = DisplayOptions.query.filter(DisplayOptions.metaLevel == 0).all()[-1]
    popTypesToView        = displayOptions.popTypes
    dispFits              = displayOptions.dispFits
    dispFitsIdx           = displayOptions.dispFitsIdx

    lastGen = Individuals.query.filter(Individuals.trial == trialToView).order_by(Individuals.idInDb.desc()).first().curGen
    inds    = Individuals.query.filter(Individuals.trial == trialToView, Individuals.curGen == lastGen).filter(Individuals.popType.in_(popTypesToView)).all()

    fittestFrontRel = RetrieveFittestFront(inds,trialToView, lastGen, popTypesToView, dispFits, dispFitsIdx)
    fittestFront    = [inds[i].id for i in fittestFrontRel] 
    #####

    if trialToViewPrev != trialToView:
        #trialDb           = Trials.query.filter_by(metaLevel = 0).all()[-1]
        trialDb           = Trials.query.filter_by(id = trialToView).all()[-1]  
        designType        = trialDb.designType

    if designType == "Coupling":
        retrievedMetas = MetaResults.query.filter(MetaResults.trial == trialToView).filter(MetaResults.indId.in_(fittestFront)).all()

        indSliderVal     = request.args.get('indMeta',0,type=float)
        selectedIndIdRel = SliderQuery(len(fittestFront),indSliderVal)
        indId            = fittestFront[selectedIndIdRel]
        ind              = inds[fittestFrontRel[selectedIndIdRel]]
        
        metaResult           = [m for m in retrievedMetas if m.indId == indId][0]   
        ampOrConvSliderValue = request.args.get('ampOrConv',0,type=float)
        ampOrConvToView      = [0,1][SliderQuery(2,ampOrConvSliderValue)]
       
        if ampOrConvToView == 0:
             trialInstanceToView = metaResult.ampTrialId
        else:
             trialInstanceToView = metaResult.convTrialId

        boolDisplayChanged = displayOptionsPrev.popTypes != displayOptions.popTypes or displayOptionsPrev.dispFits != displayOptions.dispFits
        boolRefresh = trialToViewPrev != trialToView or indIdPrev!= indId or trialInstanceToViewPrev != trialInstanceToView or boolDisplayChanged
        displayOptionsInstance = DisplayOptions.query.filter(DisplayOptions.metaLevel == 1).all()[-1]
        md = RetriveMonomersDimers(boolRefresh,trialInstanceToView, displayOptionsInstance) #This is broken now that I can specify any gen

        trialToViewPrev         = trialToView
        trialInstanceToViewPrev = trialInstanceToView
        indIdPrev               = indId
        displayOptionsPrev      = displayOptions

        dispFitsValuesMeta = [ind.fitnesses[i] for i in dispFitsIdx]
        dispFitsValuesAll  = dispFitsValuesMeta + md[1][0]
        dispFitsMeta       = ["Meta-" + f for f in dispFits]
        dispFitsAll        = dispFitsMeta + md[1][1]
        return jsonify(md[0] + (dispFitsValuesAll,dispFitsAll,1)) #The first two of these extra elements of the JSON are for the meta fitnesses. The 3rd is boolMeta
    else:
        boolDisplayChanged = displayOptionsPrev.popTypes != displayOptions.popTypes or displayOptionsPrev.dispFits != displayOptions.dispFits
        genToView = request.args.get('gen',0,type=int)
        print genToView
        boolRefresh = trialToViewPrev != trialToView or boolDisplayChanged or genToViewPrev != genToView
        md = RetriveMonomersDimers(boolRefresh,trialToView, displayOptions,genToView)

        trialToViewPrev     = trialToView
        displayOptionsPrev  = displayOptions
        genToViewPrev       = genToView

        return jsonify(md[0] + md[1] +(0,))  


def RetriveMonomersDimers(boolRefresh,trialToView,displayOptions,genToView):
    print genToView
    global retrievedMonomers
    global retrievedDimers
    global colorStrings

    if boolRefresh == 1:
        trialDb               = Trials.query.filter(Trials.id == trialToView).all()[0]
        colorStrings          = trialDb.colorStrings

    popTypesToView  = displayOptions.popTypes
    dispFits        = displayOptions.dispFits
    dispFitsIdx     = displayOptions.dispFitsIdx

    #lastGen = Individuals.query.filter(Individuals.trial == trialToView).order_by(Individuals.idInDb.desc()).first().curGen    
    inds = Individuals.query.filter(Individuals.trial == trialToView, Individuals.curGen == genToView).filter(Individuals.popType.in_(popTypesToView)).all()

    fittestFrontRel = RetrieveFittestFront(inds,trialToView, genToView, popTypesToView, dispFits, dispFitsIdx)
    fittestFront    = [inds[i].id for i in fittestFrontRel] 
    printFits = [inds[i].fitnesses for i in fittestFrontRel]
    print [list(x) for x in set(tuple(x) for x in printFits)]


    if boolRefresh == 1:
        retrievedMonomers = MonomerFoldingResults.query.filter(MonomerFoldingResults.trial == trialToView).filter(MonomerFoldingResults.indId.in_(fittestFront)).all()
        retrievedDimers   = DimerFoldingResults.query.filter(DimerFoldingResults.trial == trialToView).filter(DimerFoldingResults.indId.in_(fittestFront)).all()

    indSliderVal     = request.args.get('ind',0,type=float)
    selectedIndIdRel = SliderQuery(len(fittestFront),indSliderVal)
    indId            = fittestFront[selectedIndIdRel]
    ind              = inds[fittestFrontRel[selectedIndIdRel]]

    pairType = request.args.get('pairType',0,type=int)

    if pairType == 0: #All pairs
        monomers                    = [m for m in retrievedMonomers if m.indId == indId]      
        monomer1SliderValue         = request.args.get('mon1',0,type=float)
        monomer1                    = monomers[SliderQuery(len(monomers),monomer1SliderValue)]
        monomer2SliderValue         = request.args.get('mon2',0,type=float)
        monomer2                    = monomers[SliderQuery(len(monomers),monomer2SliderValue)]
        dimerName, dimerSeq, dimer  = RetrieveDimer(retrievedDimers,indId,monomer1,monomer2)

    elif pairType == 1: #Rz-Input pairs
        rzInputPairs                = Misc.query.filter_by(trialId = trialToView).all()[0].rzInputPairs ##TO DO -> Buffer
        pairsMetadata               = rzInputPairs.keys()
        pairTypeSliderValue         = request.args.get('rzInputPair',0,type=float)
        pairMetadata                = pairsMetadata[SliderQuery(len(pairsMetadata),pairTypeSliderValue)] 
        rzInputPair                 = rzInputPairs[pairMetadata]
        monomer1                    = [m for m in retrievedMonomers if m.indId == indId and m.name == rzInputPair[1]][0] #show the input on left
        monomer2                    = [m for m in retrievedMonomers if m.indId == indId and m.name == rzInputPair[0]][0]
        dimerName, dimerSeq, dimer  = RetrieveDimer(retrievedDimers,indId,monomer1,monomer2)

    dispFitsValues = [ind.fitnesses[i] for i in dispFitsIdx]

    if monomer1.name == 'Amp' or monomer1.name == 'Conv': colorStringMon1 = colorStrings[monomer1.name]
    else:                                                       colorStringMon1 = colorStrings['Other']
    if monomer2.name == 'Amp' or monomer2.name == 'Conv': colorStringMon2 = colorStrings[monomer2.name]
    else:                                                       colorStringMon2 = colorStrings['Other']
    if 'Amp' in (monomer1.name,monomer2.name):            colorStringDimer = colorStrings['Amp']
    elif 'Conv' in (monomer1.name,monomer2.name):         colorStringDimer = colorStrings['Conv']
    else:                                                       colorStringDimer = colorStrings['Other']
    cStrings = [colorStringMon1,colorStringMon2,colorStringDimer]

    #I don't think Tup2Str is even necessary
    return [(indId,monomer1.seq,monomer1.phenotype,Tup2Str(monomer1.name),monomer2.seq,monomer2.phenotype,
                Tup2Str(monomer2.name),dimerSeq, dimer.phenotype, dimerName,dimer.k,cStrings),(dispFitsValues,dispFits)]


def RetrieveDimer(retrievedDimers,indId,monomer1,monomer2):
    dimers = [d for d in retrievedDimers if d.indId == indId and d.name1 == monomer1.name and d.name2 == monomer2.name]
    if len(dimers) == 0: #If it can't find that entry, reverse the order of the names. This is because the EA doesn't generate duplicate pairs.
            dimers = [d for d in retrievedDimers if d.indId == indId and d.name1 == monomer2.name and d.name2 == monomer1.name]
    dimer = dimers[0]
    dimerName = Tup2Str(monomer1.name) + "&" + Tup2Str(monomer2.name)
    dimerSeq = dimer.seq1+"&"+dimer.seq2
    return (dimerName,dimerSeq,dimer)









#We get to this page by clicking on a dot in trial details
@app.route("/viewFronts",  methods=['GET'])
def RenderViewFronts():
    ###TO DO -> Add meta trial
    trialToView = request.args.get('trial',0,type=int)  
    genToView = request.args.get('gen',0,type=int)   
    return render_template('viewFronts.html', title='View Fronts', trialToView = trialToView, genToView = genToView)

#This function is called every time a user interacts on the rendered page above. It is also called  the first time the rendered page is loaded.
@app.route("/viewFronts_JSON",  methods=['GET'])
def viewFrontsJSON():

    global trialToViewPrev
    global genToViewPrev
    global popTypeToViewPrev
    global individualsAll
    global retrievedMonomers
    global retrievedDimers
    global colorString

    currentDisplayOptions = DisplayOptions.query.all()[-1]
    popTypeToView = currentDisplayOptions.popType
    dispNdSortingIndex  = currentDisplayOptions.dispNdSortingIndex 
    dispFits = currentDisplayOptions.dispFits
    dispFitsIdx = currentDisplayOptions.dispFitsIdx

    trialToView = request.args.get('trial',0,type=int)
    genToView = request.args.get('gen',0,type=int)
    indSliderVal = request.args.get('ind',0,type=float)
    frontSliderValue = request.args.get('front',0,type=float)

    if trialToViewPrev != trialToView or genToViewPrev != genToView or popTypeToViewPrev != popTypeToView:
        individualsAll = Individuals.query.filter_by(trial = trialToView, curGen = genToView, popType = popTypeToView).all()
        indIds = [i.idInTrial for i in individualsAll]
        retrievedMonomers = MonomerFoldingResults.query.filter(MonomerFoldingResults.trial == trialToView).filter(MonomerFoldingResults.popType == popTypeToView).filter(MonomerFoldingResults.indId.in_(indIds)).all()
        #retrievedMonomers = MonomerFoldingResults.query.filter(MonomerFoldingResults.trial == trialToView).filter(MonomerFoldingResults.popType == popTypeToView).filter(DimerFoldingResults.gen == genToView).all()

        print len(retrievedMonomers)
        retrievedDimers = DimerFoldingResults.query.filter(DimerFoldingResults.trial == trialToView).filter(DimerFoldingResults.popType == popTypeToView).filter(DimerFoldingResults.indId.in_(indIds)).all()
        #retrievedDimers = DimerFoldingResults.query.filter(DimerFoldingResults.trial == trialToView).filter(DimerFoldingResults.popType == popTypeToView).filter(DimerFoldingResults.gen == genToView).all()


    if trialToViewPrev != trialToView:
        trialDb = Trials.query.all()[-1]
        colorString = trialDb.colorString
    print "Finishing querying database"
    numFronts = individualsAll[0].numFrontsAllNdSortings[dispNdSortingIndex]
    frontToView = SliderQuery(numFronts,frontSliderValue)

    individualsFrontToView = [ind for ind in individualsAll if ind.frontIdAllNdSortings[dispNdSortingIndex] == frontToView]
    frontMemberToView = SliderQuery(len(individualsFrontToView),indSliderVal)

    selectedIndividual = individualsFrontToView[frontMemberToView]
    indId = selectedIndividual.idInTrial
    #indId = individualsFittestFront[frontMemberToView].idInTrial
    #selectedIndividual = Individuals.query.filter_by(trial = trialToView, idInTrial = indId, popType = popTypeToView).all()[0]


    #allMonomers = MonomerFoldingResults.query.filter_by(trial = trialToView,individualId = indId, popType = popTypeToView).all()

    allMonomers = [m for m in retrievedMonomers if m.indId == indId]
    print "Retrieved all monomers"

    ###Rearrange this list so that the input is first and the ribozyme is second to reduce the sliding I have to do
    rzPos = [id for id,monomer in enumerate(allMonomers) if 'Rz' in monomer.name][0]
    rz = allMonomers[rzPos]
    monomerAtPos1 = allMonomers[1]
    allMonomers[rzPos] = copy.deepcopy(monomerAtPos1)
    allMonomers[1] = copy.deepcopy(rz)
       
    monomer1SliderValue = request.args.get('mon1',0,type=float)
    monomer1ToView = SliderQuery(len(allMonomers),monomer1SliderValue) ###!!!CAUTION!!! If we store both OFF and POP allMonomers is twice as long
    selectedMonomer1 = allMonomers[monomer1ToView]
    monomer1Name = selectedMonomer1.name

    monomer2SliderValue = request.args.get('mon2',0,type=float)
    monomer2ToView = SliderQuery(len(allMonomers),monomer2SliderValue) 
    selectedMonomer2 = allMonomers[monomer2ToView]
    monomer2Name = selectedMonomer2.name 
  
    #dimerQueryResults = DimerFoldingResults.query.filter_by(trial = trialToView,individualId = indId, name1 = monomer1Name, name2 = monomer2Name, popType = popTypeToView).all()
    dimerQueryResults = [d for d in retrievedDimers if d.indId == indId and d.name1 == monomer1Name and d.name2 == monomer2Name]
    if len(dimerQueryResults) == 0: #If it can't find that entry, reverse the order of the names. This is because the EA doesn't generate duplicate pairs.
         #dimerQueryResults = DimerFoldingResults.query.filter_by(trial = trialToView,individualId = indId, name1 = monomer2Name, name2 = monomer1Name).all()
         dimerQueryResults = [d for d in retrievedDimers if d.indId == indId and d.name1 == monomer2Name and d.name2 == monomer1Name]
    selectedDimer = dimerQueryResults[0]
    dimerName = monomer1Name + "&" + monomer2Name
    dimerJointSeq = selectedDimer.seq1+"&"+selectedDimer.seq2
    print "Retrieved all dimers"

    dispFitsValues = [selectedIndividual.fitnesses[i] for i in dispFitsIdx]

    trialToViewPrev = trialToView
    genToViewPrev = genToView
    popTypeToViewPrev = popTypeToView

    return jsonify((dimerJointSeq, selectedDimer.phenotype, dimerName,indId,selectedMonomer1.seq,
                    selectedMonomer1.phenotype,monomer1Name,selectedMonomer2.seq,selectedMonomer2.phenotype,
                    monomer2Name,dispFitsValues,dispFits,frontToView,selectedDimer.k,colorString))


#!!! TO DO -> Handle different meta levels
@app.route("/deleteTrial",  methods=['GET'])
def DeleteTrial():
    trialToDelete = request.args.get('trial',0,type=int)  
    Trials.query.filter_by(id=trialToDelete).delete()
    Individuals.query.filter_by(trial=trialToDelete).delete()
    MonomerFoldingResults.query.filter_by(trial=trialToDelete).delete()
    DimerFoldingResults.query.filter_by(trial=trialToDelete).delete()
    db.session.commit()


    allTrials = Trials.query.filter_by(metaLevel = 0).all()
    links = ["/trialDetails?trial=" + str(trial.id) for trial in allTrials]
    return render_template('trials.html', title='Trials', trials = allTrials, links = links)

@app.route("/ractip")
def RactipPageLoad():
    return render_template('ractip.html', title='Ractip')

@app.route('/ractipResults', methods=['GET','POST'])
def ractip_form():
    a = request.args.get('a',0,type=float)
    b = request.args.get('b',0,type=float)
    t = request.args.get('t',0,type=float)
    u = request.args.get('u',0,type=float)
    accNum = request.args.get('accNum',0,type=int)
    seq1 = request.args.get('seq1')
    seq2 = request.args.get('seq2')

    params = [a,b,t,u,accNum]
    ractipResults = Ractip(seq1,seq2,params)[0]
    dbStructure = ractipResults[2]
    freeEnergy = ractipResults[0]

    dimerJointSeq = seq1+"&"+seq2
    return jsonify((dimerJointSeq, dbStructure, freeEnergy)) 


def ProcessLevel(levelString,delimeterName):
    delimeters = [('><','_lvl0_'),('}{','_lvl1_'),('][','_lvl2_')]
    currentDelimeter = [delimeter for delimeter in delimeters if delimeter[0] == delimeterName][0]
    levelCharOpen = delimeterName[1]
    levelCharClose = delimeterName[0]
    processedLevel = levelString.replace(currentDelimeter[0],currentDelimeter[1])
    processedLevel = processedLevel.replace(levelCharOpen,'')
    processedLevel = processedLevel.replace(levelCharClose,'')
    processedLevel = processedLevel.split(currentDelimeter[1])
    #print processedLevel
    return processedLevel

def ConvertDataType(elem,dataType):
    if dataType == 'float':
        convertedElem = float(elem)
    elif dataType == 'string':
        convertedElem = str(elem)
    elif dataType == 'int':
        convertedElem = int(elem)
    return convertedElem

def ConvertListDataTypes(targetList,dataType):
    convertedList = list()
    for elem in targetList:
        convertedElem = ConvertDataType(elem,dataType)
        convertedList.append(convertedElem)
    return convertedList

#only commas becomes a list of elems
#][ becomes a list of lists of elems
#}{ becomes a list of lists of lists of elems
def ReadListFromForm(formString,dataType):
    #>< is not yet completed
    if '><' in formString:     
        delimeterNames = ['><','}{','][']
    elif '}{' in formString:     
        delimeterNames = ['}{','][']
    elif '][' in formString:
        delimeterNames = ['][']
    elif ',' in formString:
        delimeterNames = []
    else:
        return [ConvertDataType(formString,dataType)] #This is if there is just a single number with no commas
        #!!! I'm making decision to return it as a list of a single element rather than just a single element

    formList = list()
    if len(delimeterNames) == 0:
        formList = ConvertListDataTypes(formString.split(','),dataType)
        return formList
    else:
        formList = ProcessLevel(formString,delimeterNames[0]) #List of strings where each string contains delimeters 1 to n

    #formListStatic = copy.deepcopy(formList) #So that we don't change list during iteration
    if len(delimeterNames) == 1:
        for i,formSubstring in enumerate(formList):    
            formList[i] = ConvertListDataTypes(formSubstring.split(','),dataType)
        return formList
    else:
        for i,formSubstring in enumerate(formList):   
            formList[i] = ProcessLevel(formSubstring,delimeterNames[1]) #changing list while mutate it can be problematic. 
            #FormList is now a list of list of strings where each string contains delimeters 2 to n

    #formListStatic = copy.deepcopy(formList) #So that we don't change list during iteration
    if len(delimeterNames) == 2:
        for i,formSubList in enumerate(formList):
            for j,formSubstring in enumerate(formSubList):            
                formList[i][j] = ConvertListDataTypes(formList[i][j].split(','),dataType)
        return formList
    else:
        for i,formSubList in enumerate(formList):
            for j,formSubstring in enumerate(formSubList):    
                formList[i][j] = ProcessLevel(formSubstring,delimeterNames[2]) #FormList is now a list of list of list of strings where each string contains delimeters 3 to n
            
def argMax(x):
    return max(enumerate(x), key=lambda x:x[1])[0]


def InsertTrial(params):
    trial = Trials(*params)
    db.session.add(trial)
    db.session.commit()

def RetrieveTrialId():
    allTrials = Trials.query.all()
    return allTrials[-1].id

'''
def ClearIndividuals():
    db.session.query(Individuals).delete()
    db.session.query(MonomerFoldingResults).delete()
    db.session.query(DimerFoldingResults).delete()
    db.session.commit()
'''
def ClearArchive():
    Individuals.query.filter_by(popType=2).delete()
    MonomerFoldingResults.query.filter_by(popType=2).delete()
    DimerFoldingResults.query.filter_by(popType=2).delete()
    db.session.commit()

    
def InsertMFoldingResults(rows):
    for row in rows:
        db.session.add(MonomerFoldingResults(*row))
    db.session.commit()

def InsertDFoldingResults(rows):
    for row in rows:
        db.session.add(DimerFoldingResults(*row))
    db.session.commit()

def InsertMetaResults(rows):
    for row in rows:
        db.session.add(MetaResults(*row))
    db.session.commit()

def InsertGenericIndividuals(rows):
    for row in rows:
        db.session.add(Individuals(*row))
    db.session.commit()

def InsertMisc(trialId,rzInputPairs):
    db.session.add(Misc(trialId,rzInputPairs))
    db.session.commit()

#run the web server
if __name__ == "__main__":
    import RNAEA,CouplingEA
    app.run(host='0.0.0.0', port='80')

